// Switch certain class of snprintf to use internal macro crm_snprintf_max
// jpokorny@redhat.com

@ has_crm_internal @
@@

#include <crm_internal.h>


@ snprintf_eligible_1 exists @
expression E1, E2;
identifier fn, id_str, id_max, id_offset;
type t_max, t_offset;
@@
fn(...) {
...
(
t_offset id_offset = E1;
...
- static t_max id_max = E2;
+ static const t_max id_max = E2;
|
- static t_max id_max = E2;
+ static const t_max id_max = E2;
...
t_offset id_offset = E1;
|
t_offset id_offset = E1;
...
t_max id_max = E2;
|
t_max id_max = E2;
...
t_offset id_offset = E1;
//|
//t_offset id_offset = E1;
//|
//t_offset id_offset = E1, id_max = E2;
//|
//t_offset id_max = E2, id_offset = E1;
)
<+...
id_offset += snprintf(id_str + id_offset, id_max - id_offset, ...);
...+>
}


@ snprintf_eligible_2 exists @
identifier fn, id_str, id_max, id_offset;
type t_max, t_offset;
@@
fn(..., t_offset id_offset, t_max id_max,...)
{
<+...
id_offset += snprintf(id_str + id_offset, id_max - id_offset, ...);
...+>
}


@ snprintf_rewrite_1 depends on snprintf_eligible_1 forall @
identifier snprintf_eligible_1.fn;
identifier snprintf_eligible_1.id_str;
identifier snprintf_eligible_1.id_max;
identifier snprintf_eligible_1.id_offset;
@@
fn(...) {
<...
id_offset +=
- snprintf(id_str + id_offset, id_max - id_offset
+ crm_snprintf_offset(id_str, id_offset, id_max
, ...);
...>
}

@ snprintf_rewrite_2 depends on snprintf_eligible_2 forall @
identifier snprintf_eligible_2.fn;
identifier snprintf_eligible_2.id_str;
identifier snprintf_eligible_2.id_max;
identifier snprintf_eligible_2.id_offset;
@@
fn(...) {
<...
id_offset +=
- snprintf(id_str + id_offset, id_max - id_offset
+ crm_snprintf_offset(id_str, id_offset, id_max
, ...);
...>
}


@ snprintf_assert_1 depends on snprintf_eligible_1 forall @
identifier snprintf_eligible_1.fn;
identifier snprintf_eligible_1.id_max;
identifier snprintf_eligible_1.id_offset;
@@
fn(...) {
<...
- CRM_LOG_ASSERT(id_offset > 0);
+ CRM_LOG_ASSERT(id_offset > 0 && id_offset < id_max);
...>
}


@ snprintf_assert_2 depends on snprintf_eligible_2 forall @
identifier snprintf_eligible_2.fn;
identifier snprintf_eligible_2.id_max;
identifier snprintf_eligible_2.id_offset;
@@
fn(...) {
<...
- CRM_LOG_ASSERT(id_offset > 0);
+ CRM_LOG_ASSERT(id_offset > 0 && id_offset < id_max);
...>
}


// this is not the best placement, it serves a subsequent manual review
// to denote necessity to add the include at all, and the reviewer will
// (hopefully) move the include to the correct location
@ snprintf_include_1 depends on (snprintf_eligible_1 && !(has_crm_internal)) exists @
identifier snprintf_eligible_1.fn;
@@
+ #include <crm_internal.h>  /* crm_snprintf_offset */

fn(...) { ... }


@ snprintf_include_2 depends on (snprintf_eligible_2 && !(has_crm_internal)) exists @
identifier snprintf_eligible_2.fn;
@@
+ #include <crm_internal.h>  /* crm_snprintf_offset */

fn(...) { ... }
