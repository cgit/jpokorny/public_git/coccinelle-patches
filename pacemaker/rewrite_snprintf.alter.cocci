// Switch certain class of snprintf to use internal macro crm_snprintf_max
// jpokorny@redhat.com

@ has_crm_internal @
@@

#include <crm_internal.h>


@ snprintf_rewrite @
identifier fn;
identifier id_str;
identifier id_max;
identifier id_offset;
@@
fn(...) {
<...
id_offset +=
- snprintf(id_str + id_offset, id_max - id_offset
+ crm_snprintf_offset(id_str, id_offset, id_max
, ...);
...>
}


@ snprintf_assert depends on snprintf_rewrite forall @
identifier snprintf_rewrite.fn;
identifier snprintf_rewrite.id_max;
identifier snprintf_rewrite.id_offset;
@@
fn(...) {
<...
- CRM_LOG_ASSERT(id_offset > 0);
+ CRM_LOG_ASSERT(id_offset > 0 && id_offset < id_max);
...>
}


// this is not the best placement, it serves a subsequent manual review
// to denote necessity to add the include at all, and the reviewer will
// (hopefully) move the include to the correct location
@ snprintf_include depends on (snprintf_rewrite && !(has_crm_internal)) exists @
identifier snprintf_rewrite.fn;
@@
+ #include <crm_internal.h>  /* crm_snprintf_offset */

fn(...) { ... }


@ snprintf_make_const depends on snprintf_rewrite forall @
identifier snprintf_rewrite.fn;
identifier snprintf_rewrite.id_max;
identifier snprintf_rewrite.id_offset;
type t_max;
expression E_max;
@@
fn(...) {
<+...
- static t_max id_max = E_max;
+ static const t_max id_max = E_max;
...+>
}
