// Fix possible NULL deref for valuePop retval (v3)
// jpokorny@redhat.com
//... when != if (<+...E == NULL...+>) S1
//    when != if (<+...E != NULL...+>) S1
//... when != \(<+...E...+>\|<+...E!=NULL && E1...+>\|<+...E==NULL || E1...+>\)
//... when != \((<+...E->item...+>)\|(<+...E->item...+>)\|(E != NULL) || E1\|(E == NULL) && E1\)
//... when != \((<+...E->item...+>)\|(<+...E=E1...+>)\|(E == NULL) && E1\|(E != NULL) || E1\)

@incl@
@@

#include <libxml/xpathInternals.h>

@voidfn depends on incl exists@
expression E, E1, E2, E3;
identifier fn, item, item2;
statement S1, S2;
@@
void fn (...) {
<...
E = valuePop(...);
+ if (E == NULL) return;
... when != if (<+...E...+>) S1
//    when != if (E->item != E1) S1
    when != E->item2 == NULL && <+... E = E1 ...+>
// specialize->    when != if (<+...E->item...+>) S1
//    when != (<+...E=E1...+>)
//    when != if (E1) {<+...E=E2...+>} S1
//    when != if (E1) S1 else {<+...E=E2...+>}
(
E->item;
|
E->item
... when != \((E == NULL) && E2\|(E != NULL) || E2\)
)
//... when != \((<+...E->item...+>)\|(E == NULL)\|(E != NULL)\)
//... when != \((<+...E->item...+>)\|(<+...E=E1...+>)\|(E == NULL) && E1\|(E != NULL) || E1\)

...>
}

@nonvoidfn depends on incl && !voidfn exists@
expression E;
identifier fn, item;
statement S1, S2;
@@
fn (...) {
<...
E = valuePop(...);
+ if (E == NULL) return NULL;
... when != if (<+...E...+>) S1
//    when != if (<+...E->item...+>) S1
(
E->item;
|
E->item
//... when != \((E == NULL) && E1\|(E != NULL) || E1\)
... when != \((<+...E=E1...+>)\|(E == NULL) || E1\|(E != NULL) && E1\)
)
...>
}
