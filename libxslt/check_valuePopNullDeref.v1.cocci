// See: https://mail.gnome.org/archives/xslt/2013-December/msg00008.html
// Fix possible NULL deref for valuePop retval
// jpokorny redhat com

@incl@
@@

#include <libxml/xpathInternals.h>

@voidfn depends on incl exists@
expression E;
identifier fn, f, item;
statement S1, S2;
@@
void fn (...) {
<...
E = valuePop(...);
+ if (E == NULL) return;
... when != if (E == NULL) S1 else S2
(
E->item;
|
E->item
)
...>
}

// for cases the function is non-void (which implicitly supposes
// a pointer as a return value rather than anything else);
// not found helpful in libxslt case presently anyway
//@nonvoidfn depends on incl exists@
//expression E;
//identifier fn != voidfn.fn, f, item;
//statement S1, S2;
//@@
//fn (...) {
//<...
//E = valuePop(...);
//+ if (E == NULL) return NULL;
//... when != if (E == NULL) S1 else S2
//(
//E->item;
//|
//E->item
//)
//...>
//}
