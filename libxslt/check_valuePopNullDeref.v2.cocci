// See: https://mail.gnome.org/archives/xslt/2013-December/msg00011.html
// Fix possible NULL deref for valuePop retval (v2)
// jpokorny redhat com

@incl@
@@

#include <libxml/xpathInternals.h>

@voidfn depends on incl exists@
expression E;
identifier fn, item;
statement S1, S2;
@@
void fn (...) {
<...
E = valuePop(...);
+ if (E == NULL) return;
... when != if (<+... E == NULL ...+>) S1 else S2
(
E->item;
|
E->item
)
...>
}

//@nonvoidfn depends on incl exists@
//expression E;
//identifier fn != voidfn.fn, item;
//statement S1, S2;
//@@
//fn (...) {
//<...
//E = valuePop(...);
//+ if (E == NULL) return NULL;
//... when != if (<+... E == NULL ...+>) S1 else S2
//(
//E->item;
//|
//E->item
//)
//...>
//}
